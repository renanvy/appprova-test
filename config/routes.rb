Rails.application.routes.draw do
  resources :dashboard, only: :index

  resources :courses, except: :show

  resources :institutions

  resources :evaluations, only: :index

  root 'dashboard#index'
end
