require 'rails_helper'

RSpec.describe Course, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:name) }
  end

  describe 'associations' do
    it { is_expected.to have_many(:evaluations).dependent(:restrict_with_exception) }
    it { is_expected.to have_many(:institutions).through(:evaluations) }
  end
end