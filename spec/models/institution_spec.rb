require 'rails_helper'

RSpec.describe Institution, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:score) }
    it { should validate_numericality_of(:score).is_greater_than_or_equal_to(0.0).is_less_than_or_equal_to(10.0) }
  end

  describe 'associations' do
    it { is_expected.to have_many(:evaluations).dependent(:destroy) }
    it { is_expected.to have_many(:courses).through(:evaluations) }
  end

  describe 'nested' do
    it { should accept_nested_attributes_for(:evaluations).allow_destroy(true) }
  end
end