require 'rails_helper'

RSpec.describe Evaluation, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:course) }
    it { should validate_presence_of(:institution) }
    it { should validate_presence_of(:students_average_score) }
    it { should validate_presence_of(:course_score) }

    it { should validate_numericality_of(:students_average_score).is_greater_than_or_equal_to(0.0).is_less_than_or_equal_to(10.0) }
    it { should validate_numericality_of(:course_score) }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:course) }
    it { is_expected.to belong_to(:institution) }
  end

  describe 'delegates' do
    it { should delegate_method(:name).to(:course).with_prefix }

    it { should delegate_method(:name).to(:institution).with_prefix }
    it { should delegate_method(:score).to(:institution).with_prefix }
  end
end