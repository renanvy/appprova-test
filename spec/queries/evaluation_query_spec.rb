require 'rails_helper'

describe EvaluationQuery do
  fixtures :evaluations

  subject { EvaluationQuery.new.all }

  let!(:evaluation_fafit_mathematics) { evaluations(:evaluation_fafit_mathematics) }
  let!(:evaluation_fait_mathematics) { evaluations(:evaluation_fait_mathematics) }
  let!(:evaluation_fait_spanish) { evaluations(:evaluation_fait_spanish) }

  describe '#institution_score_desc' do
    it 'should return evaluations ordered by institution score desc' do
      expect(subject.institution_score_desc).to eq([
        evaluation_fait_mathematics,
        evaluation_fait_spanish,
        evaluation_fafit_mathematics
      ])
    end

    it 'should not return evaluations ordered by institution score asc' do
      expect(subject.institution_score_desc).to_not eq([
        evaluation_fafit_mathematics,
        evaluation_fait_mathematics
      ])
    end
  end

  describe '#institution_name' do
    it 'should return evaluations from institution FAFIT' do
      expect(subject.institution_name('FAFIT')).to contain_exactly(evaluation_fafit_mathematics)
    end
  end

  describe '#course_name' do
    it 'should return evaluations from spanish course' do
      expect(subject.course_name('spanish')).to contain_exactly(evaluation_fait_spanish)
    end
  end

  describe '#institution_score' do
    it 'should return evaluations with institution score = 9' do
      expect(subject.institution_score(9)).to contain_exactly(evaluation_fait_spanish, evaluation_fait_mathematics)
    end
  end

  describe '#course_score' do
    it 'should return evaluations with course score = 7' do
      expect(subject.course_score(7)).to contain_exactly(evaluation_fait_mathematics)
    end
  end

  describe '#students_average_score' do
    it 'should return evaluations with students average score = 9' do
      expect(subject.students_average_score(9)).to contain_exactly(evaluation_fait_mathematics, evaluation_fait_spanish)
    end
  end
end
