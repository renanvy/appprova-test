require 'rails_helper'

feature 'Evaluations filter' do
  scenario 'Filter by institution name', js: true do
    visit '/'

    expect(page).to have_content('Dashboard')

    fill_in 'institution_name', with: 'FAFIT'

    click_on 'Filter'

    within '.table' do
      expect(page).to have_content('FAFIT')
      expect(page).to_not have_content('FAIT')
    end
  end
end