require 'rails_helper'

feature 'Edit institutions' do
  scenario 'Edit a institution with valid information', js: true do
    visit '/institutions'

    within ".table #institution_2" do
      expect(page).to have_content('FAIT')

      click_on 'Edit'
    end

    within '#institution_form' do
      fill_in 'Name', with: 'UTFRJ'

      click_on 'Save'
    end

    within '.table' do
      expect(page).to have_content('UTFRJ')
    end
  end

  scenario 'Edit a institution with invalid information', js: true do
    visit '/institutions'

    within ".table #institution_2" do
      expect(page).to have_content('FAIT')

      click_on 'Edit'
    end

    within '#institution_form' do
      fill_in 'Name', with: nil
      fill_in 'institution_score', with: nil

      click_on 'Save'
    end

    expect(page).to have_content('2 error(s) prevented this record from being saved:')
    expect(page).to have_content("NAME: can't be blank")
    expect(page).to have_content("SCORE: can't be blank")
  end
end