require 'rails_helper'

feature 'Create institutions' do
  scenario 'Create a new institution with valid information', js: true do
    visit '/institutions'

    click_link 'New Institution'

    within '#institution_form' do
      fill_in 'Name', with: 'UTFPR'
      fill_in 'Score', with: 9

      within '#evaluation_form' do
        click_on '[+] Add more'

        select 'Spanish', from: 'Course'
        fill_in 'Course Score', with: 9
        fill_in 'Students Average Score', with: 8
      end

      click_on 'Save'
    end

    within '.table' do
      expect(page).to have_content('UTFPR')
    end
  end

  scenario 'Create a new institution with invalid information', js: true do
    visit '/institutions'

    click_link 'New Institution'

    within '#institution_form' do
      fill_in 'Name', with: nil
      fill_in 'Score', with: nil

      click_on 'Save'
    end

    expect(page).to have_content('2 error(s) prevented this record from being saved:')
    expect(page).to have_content("NAME: can't be blank")
    expect(page).to have_content("SCORE: can't be blank")
  end
end