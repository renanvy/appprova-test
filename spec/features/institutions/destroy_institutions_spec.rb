require 'rails_helper'

feature 'Destroy institutions' do
  scenario 'Destroy a institution' do
    visit '/'

    click_link 'Institutions'

    within ".table #institution_2" do
      expect(page).to have_content('FAIT')

      click_on 'Remove'
    end

    expect(page).to have_content('Institution successful removed.')
  end
end