require 'rails_helper'

feature 'List institutions' do
  scenario 'List all institutions' do
    visit '/'

    click_link 'Institutions'

    within '.table' do
      expect(page).to have_content('FAFIT')
      expect(page).to have_content('FAIT')
    end
  end
end