require 'rails_helper'

feature 'Destroy courses' do
  scenario 'Destroy a course' do
    visit '/'

    click_link 'Courses'

    within ".table #course_3" do
      expect(page).to have_content('English')

      click_on 'Remove'
    end

    expect(page).to have_content('Course successful removed.')
  end
end