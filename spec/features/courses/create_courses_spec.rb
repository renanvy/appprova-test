require 'rails_helper'

feature 'Create courses' do
  scenario 'Create a new course with valid information' do
    visit '/'

    click_link 'Courses'

    click_link 'New Course'

    within '.new_course' do
      fill_in 'Name', with: 'Physical Education'

      click_on 'Create Course'
    end

    expect(page).to have_content('Course successful created.')
  end

  scenario 'Create a new course with invalid information' do
    visit '/'

    click_link 'Courses'

    click_link 'New Course'

    within '.new_course' do
      fill_in 'Name', with: nil

      click_on 'Create Course'
    end

    expect(page).to have_content('1 error prevented this record from being saved:')
    expect(page).to have_content("Name can't be blank")
  end
end