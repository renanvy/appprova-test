require 'rails_helper'

feature 'Edit courses' do
  scenario 'Edit a course with valid information' do
    visit '/'

    click_link 'Courses'

    within ".table #course_2" do
      expect(page).to have_content('Spanish')

      click_on 'Edit'
    end

    within '#edit_course_2' do
      fill_in 'Name', with: 'Physical Education'

      click_on 'Update Course'
    end

    expect(page).to have_content('Course successful updated.')
  end

  scenario 'Edit a course with invalid information' do
    visit '/'

    click_link 'Courses'

    within ".table #course_2" do
      expect(page).to have_content('Spanish')

      click_on 'Edit'
    end

    within '#edit_course_2' do
      fill_in 'Name', with: nil

      click_on 'Update Course'
    end

    expect(page).to have_content('1 error prevented this record from being saved:')
    expect(page).to have_content("Name can't be blank")
  end
end