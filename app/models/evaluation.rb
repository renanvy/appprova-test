class Evaluation < ApplicationRecord
  include Filterable

  belongs_to :course
  belongs_to :institution

  validates :course, :institution, :students_average_score, :course_score, presence: true
  validates :students_average_score, :course_score,
    numericality: { greater_than_or_equal_to: 0.0, less_than_or_equal_to: 10.0 }

  delegate :name, to: :course, prefix: true
  delegate :name, :score, to: :institution, prefix: true
end
