class Institution < ApplicationRecord
  has_many :evaluations, dependent: :destroy
  has_many :courses, through: :evaluations

  validates :name, presence: true
  validates :score, presence: true, numericality: { greater_than_or_equal_to: 0.0, less_than_or_equal_to: 10.0 }

  accepts_nested_attributes_for :evaluations, reject_if: :all_blank, allow_destroy: true
end
