class Course < ApplicationRecord
  has_many :evaluations, dependent: :restrict_with_exception
  has_many :institutions, through: :evaluations

  validates :name, presence: true
end
