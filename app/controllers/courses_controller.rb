class CoursesController < ApplicationController
  def index
    if params[:without_pagination].present?
      @courses = Course.all
    else
      @courses = Course.all.paginate(page: params[:page], per_page: 30)
    end

    respond_with @courses
  end

  def new
    @course = Course.new
  end

  def edit
    @course = Course.find(params[:id])
  end

  def create
    @course = Course.new(course_params)

    if @course.save
      redirect_to courses_url, notice: 'Course successful created.'
    else
      render :new
    end
  end

  def update
    @course = Course.find(params[:id])

    if @course.update(course_params)
      redirect_to courses_url, notice: 'Course successful updated.'
    else
      render :edit
    end
  end

  def destroy
    @course = Course.find(params[:id])

    @course.destroy

    redirect_to courses_url, notice: 'Course successful removed.'
  end

  def destroy
    @course = Course.find(params[:id])

    begin
      @course.destroy
      redirect_to courses_url, notice: 'Course successful removed.'
    rescue ActiveRecord::DeleteRestrictionError
      redirect_to(
        courses_url(@course),
        alert: 'Could not remove. Records depend on this course.'
      )
    end
  end

  private

  def course_params
    params.require(:course).permit(:name)
  end
end