class InstitutionsController < ApplicationController
  def index
    @institutions = Institution.all.paginate(page: params[:page], per_page: 30)
  end

  def show
    @institution = Institution.find(params[:id])

    respond_with @institution
  end

  def new
    @institution = Institution.new
  end

  def edit
    @institution = Institution.find(params[:id])
  end

  def create
    @institution = Institution.new(institution_params)

    @institution.save

    respond_with @institution
  end

  def update
    @institution = Institution.find(params[:id])

    @institution.update(institution_params)

    respond_with @institution
  end

  def destroy
    @institution = Institution.find(params[:id])

    @institution.destroy

    redirect_to institutions_url, notice: 'Institution successful removed.'
  end

  private

  def institution_params
    params.require(:institution).permit(
      :name,
      :score,
      evaluations_attributes: [
        :id,
        :course_id,
        :course_score,
        :students_average_score,
        :_destroy
      ]
    )
  end
end
