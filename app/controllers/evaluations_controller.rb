class EvaluationsController < ApplicationController
  def index
    @evaluations =
      evaluation_query
        .all
        .filter(
          params.slice(
            :institution_name,
            :course_name,
            :institution_score,
            :course_score,
            :students_average_score
          )
        )
        .includes(:course, :institution)
        .institution_score_desc

    respond_with @evaluations
  end

  private

  def evaluation_query
    @evaluation_query ||= EvaluationQuery.new
  end
end