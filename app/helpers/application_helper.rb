module ApplicationHelper
  def active_link_if_controller(name)
    return if controller_name != name

    'active'
  end
end
