class EvaluationQuery
  def initialize(relation = Evaluation.all)
    @relation = relation.extending(Scopes)
  end

  def all
    @relation
  end

  module Scopes
    def institution_score_desc
      joins(:institution).order(Institution.arel_table[:score].desc)
    end

    def institution_name(name)
      joins(:institution)
      .where("unaccent(#{Institution.quoted_table_name}.name) ILIKE unaccent(?)", "%#{name}%")
    end

    def course_name(name)
      joins(:course)
      .where("unaccent(#{Course.quoted_table_name}.name) ILIKE unaccent(?)", "%#{name}%")
    end

    def institution_score(score)
      joins(:institution).where(Institution.arel_table[:score].eq(score))
    end

    def course_score(score)
      where(course_score: score)
    end

    def students_average_score(score)
      where(students_average_score: score)
    end
  end
end
