class EvaluationSerializer < ActiveModel::Serializer
  attributes :id, :course_id, :course_name, :course_score, :institution_id,
    :institution_name, :institution_score, :students_average_score
end
