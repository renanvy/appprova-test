class InstitutionSerializer < ActiveModel::Serializer
  attributes :id, :name, :score

  has_many :evaluations
end
