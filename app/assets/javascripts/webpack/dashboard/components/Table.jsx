import React, { Component } from 'react'

export default class Table extends Component {
  renderData() {
    if (!this.props.data.length) {
      return (
        <tr className="text-center">
          <td colSpan="5">Nothing Found.</td>
        </tr>
      )
    }

    return this.props.data.map(evaluation => (
      <tr key={`evaluation-${evaluation.id}`}>
        <td>
          <a href={`/institutions/${evaluation.institution_id}/edit`}>
            {evaluation.institution_name}
          </a>
        </td>
        <td>{evaluation.institution_score}</td>
        <td>{evaluation.course_name}</td>
        <td>{evaluation.course_score}</td>
        <td>{evaluation.students_average_score}</td>
      </tr>
    ))
  }

  render() {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Institution</th>
            <th>Institution Score</th>
            <th>Course</th>
            <th>Course Score</th>
            <th>Students Average Score</th>
          </tr>
        </thead>

        <tbody>
          {this.renderData()}
        </tbody>
      </table>
    )
  }
}