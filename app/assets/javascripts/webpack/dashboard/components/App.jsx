import React, { Component } from 'react'

import Filter from './Filter'
import Table from './Table'

export default class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      data: [],
      loading: false
    }
  }

  componentDidMount() {
    this.fetchEvaluations()
  }

  fetchEvaluations = (params = {}) => {
    this.setState({ loading: true })

    $.ajax({
      method: 'get',
      url: '/evaluations',
      dataType: 'json',
      data: params,
      success: (data) => {
        this.setState({ loading: false, data: data })
      },
      error: () => {
        this.setState({ loading: false })

        console.log('Error')
      }
    })
  }

  filter = (evt, params) => {
    evt.preventDefault()

    this.fetchEvaluations(params)
  }

  render() {
    return (
      <div>
        <h3>Dashboard</h3>

        <Filter
          filter={this.filter}
          loading={this.state.loading}
        />

        <Table data={this.state.data} />
      </div>
    )
  }
}