import React, { Component } from 'react'

export default class Filter extends Component {
  constructor(props) {
    super(props)

    this.state = {
      params: {}
    }
  }

  update(params) {
    this.setState({
      params: { ...this.state.params, ...params }
    })
  }

  render() {
    return (
      <nav className="navbar navbar-light bg-light">
        <form onSubmit={evt => this.props.filter(evt, this.state.params)} className="form-inline">
          <input
            type="text"
            id="institution_name"
            placeholder="Institution Name"
            className="form-control form-control-sm mr-sm-2"
            onChange={evt => this.update({ institution_name: evt.target.value })}
            value={this.state.params.institution_name || ''} />

          <input
            type="number"
            step="any"
            placeholder="Institution Score"
            className="form-control form-control-sm mr-sm-2"
            onChange={evt => this.update({ institution_score: evt.target.value })}
            value={this.state.params.institution_score || ''} />

          <input
            type="text"
            placeholder="Course Name"
            className="form-control form-control-sm mr-sm-2"
            onChange={evt => this.update({ course_name: evt.target.value })}
            value={this.state.params.course_name || ''} />

          <input
            type="number"
            step="any"
            placeholder="Course Score"
            className="form-control form-control-sm mr-sm-2"
            onChange={evt => this.update({ course_score: evt.target.value })}
            value={this.state.params.course_score || ''} />

          <input
            type="number"
            step="any"
            placeholder="Students Average Score"
            className="form-control form-control-sm mr-sm-2"
            onChange={evt => this.update({ students_average_score: evt.target.value })}
            value={this.state.params.students_average_score || ''} />

          <button
            type="submit"
            disabled={this.props.loading}
            className="btn btn-success btn-sm mr-sm-2">
            {this.props.loading ? 'Filtering...' : 'Filter'}
          </button>
        </form>
      </nav>
    )
  }
}