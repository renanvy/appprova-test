import React, { Component } from 'react'

import EvaluationField from './EvaluationField'

export default class EvaluationFields extends Component {
  renderFields() {
    if (!this.props.data.length) { return null }

    return this.props.data.filter(field => !field._destroy).map((field) => (
      <EvaluationField
        data={field}
        key={`evaluation-field-${field.cid}`}
        remove={this.props.remove}
        update={this.props.update}
        courses={this.props.courses}
        fetchingCourses={this.props.fetchingCourses}
      />
    ))
  }

  render() {
    return (
      <div className="card" id="evaluation_form">
        <div className="card-header">
          Evaluations
        </div>

        <div className="container">
          {this.renderFields()}

          <button
            type="button"
            className="btn btn-primary"
            onClick={() => this.props.add()}>
            [+] Add more
          </button>
        </div>
        <br />
      </div>
    )
  }
}