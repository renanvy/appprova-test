import React, { Component } from 'react'

import Form from './Form'

let evaluationFieldsCounter = 0

function prepareInitialState (state) {
  state = {
    ...state,
    errors: {}
  }

  state.evaluations =
    state.evaluations.map(evaluation => {
      return {
        ...evaluation,
        cid: evaluationFieldsCounter++
      }
    })

  return state
}

function stateToRails (state) {
  let json = {
    ...state,
    evaluations_attributes: state.evaluations
  }

  delete json.evaluations

  return JSON.parse(JSON.stringify(json))
}

export default class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      fetchingInstitution: false,
      fetchingCourses: false,
      courses: [],
      data: {
        score: null,
        name: null,
        errors: {},
        evaluations: []
      }
    }
  }

  componentDidMount() {
    this.fetchCourses()

    if (this.props.institutionId) {
      this.fetchInstitution()
    }
  }

  renderPageTitle() {
    if (this.props.institutionId) {
      return <h3>Edit Institution</h3>
    }

    return <h3>New Institutition</h3>
  }

  updateEvaluation = (payload) => {
    let evaluations =
      this.state.data.evaluations.map(evaluation => {
        if (evaluation.cid === payload.cid) {
          return { ...evaluation, ...payload }
        } else {
          return evaluation
        }
      })

    this.setState({
      data: {
        ...this.state.data,
        evaluations: evaluations
      }
    })
  }

  addEvaluation = () => {
    const evaluations =
      this.state.data.evaluations.concat({
        cid: evaluationFieldsCounter++,
        course_id: null,
        course_score: null,
        students_average_score: null
      })

    this.setState({
      data: {
        ...this.state.data,
        evaluations: evaluations
      }
    })
  }

  removeEvaluation = (cid) => {
    const evaluation = this.state.data.evaluations.find(evaluation => evaluation.cid === cid)

    const payload = {
      ...evaluation,
      _destroy: true
    }

    this.updateEvaluation(payload)
  }

  update = (data) => {
    this.setState({
      data: { ...this.state.data, ...data }
    })
  }

  submit = (evt) => {
    evt.preventDefault()

    const institutionId = this.props.institutionId
    const method = institutionId ? 'put' : 'post'
    const url = institutionId ? `/institutions/${institutionId}` : '/institutions'

    this.setState({ loading: true })

    $.ajax({
      method: method,
      url: url,
      data: { institution: stateToRails(this.state.data) },
      dataType: 'json',
      success: (data) => {
        setTimeout(() => {
          window.location = '/institutions'
        }, 1000)
      },
      error: (jQXHR) => {
        this.setState({ loading: false })

        this.update({ errors: jQXHR.responseJSON.errors })
      }
    })
  }

  fetchInstitution() {
    this.setState({ fetchingInstitution: true })

    $.ajax({
      method: 'get',
      url: `/institutions/${this.props.institutionId}`,
      dataType: 'json',
      success: (data) => {
        this.setState({ fetchingInstitution: false, data: prepareInitialState(data) })
      },
      error: (jQXHR) => {
        this.setState({ fetchingInstitution: false })

        console.log('Error')
      }
    })
  }

  fetchCourses() {
    this.setState({ fetchingCourses: true })

    $.ajax({
      method: 'get',
      url: `/courses?without_pagination=true`,
      dataType: 'json',
      success: (data) => {
        this.setState({ fetchingCourses: false, courses: data })
      },
      error: (jQXHR) => {
        this.setState({ fetchingCourses: false })

        console.log('Error')
      }
    })
  }

  render() {
    if (this.state.fetchingInstitution) { return null }

    return(
      <div>
        {this.renderPageTitle()}

        <Form
          addEvaluation={this.addEvaluation}
          removeEvaluation={this.removeEvaluation}
          updateEvaluation={this.updateEvaluation}
          submit={this.submit}
          update={this.update}
          data={this.state.data}
          loading={this.state.loading}
          fetchingCourses={this.state.fetchingCourses}
          courses={this.state.courses}
        />
      </div>
    )
  }
}