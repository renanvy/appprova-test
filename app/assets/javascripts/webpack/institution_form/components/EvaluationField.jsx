import React, { Component } from 'react'

export default class EvaluationField extends Component {
  renderOptionsForCoursesSelect() {
    return this.props.courses.map(course => (
      <option key={`course-${course.id}`} value={course.id}>{course.name}</option>
    ))
  }

  render() {
    return(
      <div className="row">
        <div className="col-md-4">
          <div className="form-group">
            <label htmlFor="course_id">Course</label>
            <select
              className="form-control"
              id="course_id"
              disabled={this.props.fetchingCourses}
              value={this.props.data.course_id || ''}
              onChange={evt => this.props.update({
                cid: this.props.data.cid,
                course_id: evt.target.value
              })}
              >
              <option>{this.props.fetchingCourses ? 'Carregando...' : 'Select a course'}</option>
              {this.renderOptionsForCoursesSelect()}
            </select>
          </div>
        </div>

        <div className="col-md-2">
          <div className="form-group">
            <label htmlFor="course_score">Course Score</label>
            <input
              type="text"
              className="form-control"
              id="course_score"
              value={this.props.data.course_score || ''}
              onChange={evt => this.props.update({
                cid: this.props.data.cid,
                course_score: evt.target.value
              })}
            />
          </div>
        </div>

        <div className="col-md-3">
          <div className="form-group">
            <label htmlFor="students_average_score">Students Average Score</label>
            <input
              type="text"
              className="form-control"
              id="students_average_score"
              value={this.props.data.students_average_score || ''}
              onChange={evt => this.props.update({
                cid: this.props.data.cid,
                students_average_score: evt.target.value
              })}
            />
          </div>
        </div>

        <div className="col-md-2">
          <button
            type="button"
            className="btn btn-danger"
            style={{ marginTop: 30 }}
            onClick={() => this.props.remove(this.props.data.cid)}>
            Remove
          </button>
        </div>
      </div>
    )
  }
}
