import React, { Component } from 'react'

export default class ErrorMessages extends Component {
  render() {
    if (!Object.keys(this.props.errors).length) { return null }

    return(
      <div className="alert alert-danger">
        <p>
          <strong>
            {Object.keys(this.props.errors).length} error(s) prevented this record from being saved:
          </strong>
        </p>
        <ul>
          {Object.keys(this.props.errors).map((key, value) => (
            <li key={`error-${key}`}>
              {key.toUpperCase()}: {this.props.errors[key][0]}
            </li>
          ))}
        </ul>
      </div>
    )
  }
}