import React, { Component } from 'react'

import EvaluationFields from './EvaluationFields'
import ErrorMessages from './ErrorMessages'

export default class Form extends Component {
  render() {
    return(
      <form onSubmit={evt => this.props.submit(evt)} id="institution_form">
        <ErrorMessages errors={this.props.data.errors} />

        <div className="row">
          <div className="col-md-4">
            <div className="form-group">
              <label htmlFor="institution_name">* Name</label>
              <input
                type="text"
                className="form-control"
                id="institution_name"
                onChange={evt => this.props.update({ name: evt.target.value })}
                value={this.props.data.name || ''}
              />
            </div>
          </div>

          <div className="col-md-2">
            <div className="form-group">
              <label htmlFor="institution_score">* Score</label>
              <input
                type="text"
                className="form-control"
                id="institution_score"
                onChange={evt => this.props.update({ score: evt.target.value })}
                value={this.props.data.score || ''}
              />
            </div>
          </div>
        </div>

        <EvaluationFields
          data={this.props.data.evaluations}
          remove={this.props.removeEvaluation}
          add={this.props.addEvaluation}
          update={this.props.updateEvaluation}
          courses={this.props.courses}
          fetchingCourses={this.props.fetchingCourses}
        />

        <hr />

        <button
          type="submit"
          className="btn btn-success"
          disabled={this.props.loading}>
          {this.props.loading ? 'Saving...' : 'Save'}
        </button>&nbsp;

        <a href="/institutions" className="btn btn-secondary">
          Back
        </a>
      </form>
    )
  }
}