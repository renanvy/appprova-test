class CreateInstitutions < ActiveRecord::Migration[5.1]
  def change
    create_table :institutions do |t|
      t.string :name, null: false
      t.decimal :score, precision: 14, scale: 2, null: false

      t.timestamps
    end
  end
end
