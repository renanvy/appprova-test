class CreateEvaluations < ActiveRecord::Migration[5.1]
  def change
    create_table :evaluations do |t|
      t.references :course, foreign_key: true, null: false
      t.references :institution, foreign_key: true, null: false
      t.decimal :course_score, precision: 14, scale: 2, null: false
      t.decimal :students_average_score, precision: 14, scale: 2, null: false

      t.timestamps
    end
  end
end
